ansible all -i hosts.txt -m ping
ansible web -i hosts.txt --become -m yum -a "name=nano state=present"
ansible all -i hosts.txt --become -m selinux -a "policy=targeted state=permissive"