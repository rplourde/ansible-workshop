# Lab 3 - Template
## Objectif 

Copiez et modifiez le playbook de votre lab #1 dans le répertoire lab3 et renommez-lab3.yaml

``` cp lab1-configurer-serveur-web/lab1.yaml lab3-template/lab3.yaml ```

pour ajouter ce qui suit :

- Utilisez le fichier debug.yml (voir la diapo suivante) pour explorer tous les faits d’Ansible 
- Convertissez votre fichier MOTD dans une template Jinja2  indiquant : « Bienvenue sur [hostname] ! »   
- Convertissez votre fichier index.html en une template Jinja2 pour afficher l’information suivante :
  
Serveurs Web

rhel1 192.168.3.52 - mémoire libre : 337.43 MB

- Commettez votre playbook  
